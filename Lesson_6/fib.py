# fib.py

def fib(n):
    a, b = 0, 1
    while b < n:
        print(b, end='')
        a, b = b, a + b
    print()

def fib2(n):
    result = []
    a, b = 0, 1
    while b < n:
        result.append(b)
        a, b = b, a + b
    print(result)
    result_str_1 = ', '.join(str(elem) for elem in result)
    print(result_str_1)
    result_str_2 = '; '.join(map(str, result))
    print(result_str_2)

print("##################################################")
print("fib")
fib(256)
print("##################################################")
print("fib2")
fib2(512)
