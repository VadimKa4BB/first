import sys, pkgutil
search_path = (sys.path)
all_modules = [x[1] for x in pkgutil.iter_modules(path=search_path)]
print('\n'.join(map(str, all_modules)))

# Использование объектов из импортированного модуля или пакета
# from packA.subA import sa1
# или то же самое
# import packA.subA.sa1 as sa1
# Для использования функции нам нужно добавить перед её именем имя модуля: x = sa1.helloWorld().
# При таком подходе становится ясно, из какого модуля взялась та или иная функция.