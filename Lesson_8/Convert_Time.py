import time
import datetime
timestamp = datetime.datetime.fromtimestamp(time.time())
print(timestamp.strftime('%Y.%m.%d %H:%M:%S'))
timestamp = datetime.datetime.fromtimestamp(0)
print(timestamp.strftime('%Y.%m.%d %H:%M:%S'))