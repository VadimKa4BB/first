month = int(input('Enter month number: '))
if month not in [1,2,3,4,5,6,7,8,9,10,11,12]:
    print("Month with number {month} is not found.")
    exit(0)
elif month in [1,2,12]:
    sign: str = "Winter"
elif month in [3,4,5]:
    sign: str = "Spring"
elif month in [6,7,8]:
    sign: str = "Summer"
elif month in [9,10,11]:
    sign: str = "Autumn"
print(f'{month} is a {sign}')
