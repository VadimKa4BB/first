for i in 'hello world':
    if i == "d":
        break
print(i * 2, end='')

print('')

for i in 'hello world':
    if i == 'a':
        break
else:
    print('Буквы a в строке нет')
