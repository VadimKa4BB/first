num = int(input('Enter number: '))
if num > 0:
    sign: str = "positive"
elif num == 0:
    sign: str = 'zero'
else:
    sign: str = 'negative'
print(f'{num} is a {sign} number.')

# You can do
# print('{num} is a {sign} number.'.format(num=num, sign=sign))
