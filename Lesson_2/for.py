acc = 0
for i in range(10):
    acc = acc + i
print('acc = ', acc)

print(list(range(7)))
# [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

print(list(range(4, 10)))
# [5, 6, 7, 8, 9]

print(list(range(2, 10, 3)))
# [5, 7, 9]

print(list(range(10,3, -2)))
